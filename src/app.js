import './assets/scss/app.scss';
import $ from 'cash-dom';

export const EVENT_TYPES = ['PullRequestEvent', 'PullRequestReviewCommentEvent'];

export class App {
  initializeApp () {
    const self = this;

    $('.load-username').on('click', function (e) {
      $('.username.input').removeClass('is-danger');
      const userName = $('.username.input').val();

      if (/^[a-z0-9_-]+$/.exec(userName)) {
        $('#spinner').removeClass('is-hidden');
        fetch('https://api.github.com/users/' + userName)
          .then(response => response.json())
          .then(body => {
            self.profile = body;
            self.updateProfile();
            self.getProfileHistory(userName);
          });
      } else {
        $('.username.input').addClass('is-danger');
      }
    });
  }

  getProfileHistory (userName) {
    if (userName) {
      fetch(`https://api.github.com/users/${userName}/events/public`)
        .then(response => response.json())
        .then(body => {
          this.profileHistory = body;
          this.updateProfileHistory();
        });
    }
  }

  updateProfile () {
    $('#profile-name').text($('.username.input').val());
    $('#profile-image').attr('src', this.profile.avatar_url);
    $('#profile-url')
      .attr('href', this.profile.html_url)
      .text(this.profile.login);
    $('#profile-bio>p').text(this.profile.bio || '(no information)');
  }

  updateProfileHistory () {
    if (this.profileHistory && Array.isArray(this.profileHistory) && this.profileHistory.length > 0) {
      const history = this.profileHistory.filter(el => EVENT_TYPES.includes(el.type));

      if (history.length > 0) {
        history.map(el => {
          let headingDate = '';
          if (el.created_at) {
            headingDate = new Date(el.created_at).toLocaleString('en-En', { month: 'short', day: 'numeric', year: 'numeric' });
          }

          $('#user-timeline').append(`<div class="timeline-item">
          <div class="timeline-marker"></div>
          <div class="timeline-content">
            <p class="heading">${headingDate}</p>
            <div class="content">
              <span class="gh-username">
                <img src="${el.actor.avatar_url}" />
                <a href="${el.actor.url}">${el.actor.login}</a>
              </span>
              ${el.payload && el.payload.action ? el.payload.action : ''}
              ${el.type === 'PullRequestReviewCommentEvent' ? `
              <a href="${el.payload && el.payload.comment && el.payload.comment.html_url ? el.payload.comment.html_url : ''}">comment</a>
              to
              ` : ''}
              <a href="${el.payload && el.payload.pull_request && el.payload.pull_request.html_url ? el.payload.pull_request.html_url : ''}">pull request</a>
              <p class="repo-name">
                <a href="${el.repo && el.repo.url ? el.repo.url : ''}">${el.repo && el.repo.name ? el.repo.name : ''}</a>
              </p>
            </div>
          </div>
        </div>`);
        });
      } else {
        $('#user-timeline').html('<p>(no history)</p>');
      }
    }
    $('#spinner').addClass('is-hidden');
  }
}
